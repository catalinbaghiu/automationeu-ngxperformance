import getopt
import sys
import os
import socket
import urllib
import tarfile
import threading
import Queue
import Common

RNA = "NA"
REU = "EU"
RCN = "CN"
RSA = "SA"
RANZ = "ANZ"
RTUR = "TUR"
RMEA = "MEA"
RKOR = "KOR"

lock = threading.Lock()

class myQueue(threading.Thread):
    def __init__(self,queue):
        threading.Thread.__init__(self)
        self._queue = queue
    def run(self):
        while True:
            content = self._queue.get()
            print "content is:" + content
            if isinstance(content,str) and content == "quit":
                break
            contentArray = content.split(",")
            lock.acquire()
            if not os.path.exists(contentArray[1]):
                os.makedirs(contentArray[1])
            lock.release()
            downloadSinglePackage(contentArray[0],contentArray[1],contentArray[2])
        print 'complete assign queue'

def build_pool(queue, size):
    workers = []
    for _ in range(size):
        worker = myQueue(queue)
        worker.start()
        workers.append(worker)
    return workers

def process(a,b,c):
    per = 100.0*a*b/c
    if per >100:
        per = 100
    print '%.1f%%' % per

def download_data_via_s3(url, dest, prefix):
    bucket =  url.split(prefix + '/')[-1]
    command = 'aws s3 cp s3://' + bucket + ' ' + dest + ' --region ' + Common.AWS_KOREA_REGION
    print 'command is :' + command
    os.system(command)

def downloadSinglePackage(url, dest, region, timeout=10800 ):
    if len(url) == 0:
        return
    print 'prepare to download data for ' + region
    socket.setdefaulttimeout(timeout)
    component = url.split("/")[-1]
    componentTar = os.path.splitext(component)[-2]

    workdir = dest
    if region in ('SK','KOR'):
        download_data_via_s3(url,workdir + os.sep + component,'s3.ap-northeast-2.amazonaws.com')
    else:
        urllib.urlretrieve(url,workdir + os.sep + component,process)
    print 'download ' + component + ' data file finished'

    if os.path.isfile(workdir + os.sep + component):
        print 'prepare to extract gz file'
        try:
            tarGz = tarfile.open(workdir + os.sep + component)
            names = tarGz.getnames()
            for name in names:
                tarGz.extract(name, workdir + os.sep + region)
            print 'extract region na done'
        except Exception , details:
            print str(details)
        tarGz.close()

    print 'Start to delete gz file'
    if os.path.isfile(workdir+ os.sep + component):
        try:
            print 'delete ' + component
            os.remove(workdir + os.sep + component)
        except Exception , details:
            print str(details)
    print 'Start to delete other files'
    try:
        if os.path.isfile(workdir + os.sep + 'md5manifest.txt'):
            os.remove(workdir + os.sep + 'md5manifest.txt')
        if os.path.isfile(workdir + os.sep + component + '.md5'):
            os.remove(workdir + os.sep + component + '.md5')
        if(os.path.isfile(workdir + os.sep + componentTar)):
            print "delete " + componentTar
            os.remove(workdir + os.sep + componentTar)
    except Exception , details:
        print str(details)

def downloadPackages(dict,dest):
    queue = Queue.Queue()
    worker_threads = build_pool(queue,len(dict))

    for (key,value) in dict.iteritems():
        queue.put(value + "," + dest + "," + key)
    for worker in worker_threads:
        queue.put('quit')
    for worker in worker_threads:
        worker.join()

if __name__== '__main__':

    dest = ""
    dict = {}
    try:
        shortargs = "d:n:e:c:s:a:t:m:k:"
        longargs = ["dest=","na=","eu=","cn=","sa=","anz=","tur=","mea=","kor="]
        opts, args = getopt.getopt(sys.argv[1:],shortargs,longargs)
    except getopt.GetoptError, err:
        print str(err)
        print sys.exit(0)

    for o, a in opts:
        if o in ("-d", "dest="):
            dest = a
        elif o in ("-n", "na="):
            dict[RNA] = a
        elif o in ("-e", "eu="):
            dict[REU] = a
        elif o in ("-c", "cn="):
            dict[RCN] = a
        elif o in ("-s", "sa="):
            dict[RSA] = a
        elif o in ("-a", "anz="):
            dict[RANZ] = a
        elif o in ("-t", "tur="):
            dict[RTUR] = a
        elif o in ("-m", "mea="):
            dict[RMEA] = a
        elif o in ("-k", "kor="):
            dict[RKOR] = a
        else:
            print 'unsupported option'
            sys.exit(0)
    downloadPackages(dict,dest)