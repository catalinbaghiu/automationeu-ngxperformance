
import os
import argparse
from TelnetConnection import *
from BenchProfile import *

def androidRunner(modelYear):
    rootUser = " -c "
    if modelYear.lower() != "my18":
        rootUser = " 0 "
    
    runnerDir = "/sdcard/ngx/build"
    runnerFile = "android_runner.sh"
    systemRunner = "/system/RegEx"
    cregConfig = "/sdcard/ngx/cases/cregex_config.xml"
    os.system("adb push " + runnerFile + " " + runnerDir + "/" + runnerFile)
    os.system('adb shell "su ' + rootUser + ' sh ' + runnerDir + '/' + runnerFile + '"')
    os.system('adb shell "su ' + rootUser + ' chmod 777 ' + systemRunner + '"')
    os.system('adb shell "su ' + rootUser + systemRunner + ' ' + cregConfig + '"')

def qnxRunner(project):
    runnerDir = "/fs/images/ngx"
    systemRunner = runnerDir + "/build/RegEx"
    cregConfig = runnerDir + "/cases/cregex_config.xml"
    hostname, username, password, finish = getProfile(project)
    #print hostname, username, password, finish, "chmod 777 " + systemRunner, systemRunner + ' ' + cregConfig
    do_telnet(hostname, username, password, finish, ["chmod 777 " + systemRunner])
    do_telnet(hostname, username, password, finish, [systemRunner + ' ' + cregConfig])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--project", required=True, help="gen3 or denali")
    parser.add_argument("--modelYear", required=True, help="model year: MY18 or MY19 ...")
    args = parser.parse_args()

    project = args.project.strip()
    modelYear = args.modelYear.strip()
    if project.lower() == "denali":
        androidRunner(modelYear)
    elif project.lower() == "gen3":
        qnxRunner(project.lower())
    else:
        exit("We are not supporting this project: " + project)

