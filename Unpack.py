import argparse
import logging
import os

import Utils


def parse_extension_set(extensions):
    logging.info('Parse acceptable extension set: {0}'.format(extensions))

    extension_split = ','

    extension_set = set()
    if extension_split in extensions:
        for extension in extensions.split(extension_split):
            extension_set.add(extension)
    else:
        extension_set.add(extensions)
    
    logging.info('Extension set is: {0}'.format(extension_set))
    return extension_set


def find_packaged_files(from_dir, extensions):
    logging.info('Finding packaged files from dir: {0} with extensions: {1}'.format(from_dir, extensions))

    packaged_files = []
    extension_set = parse_extension_set(extensions)
    for root, dirs, files in os.walk(from_dir):
        for filename in files:
            if Utils.endswith_set(filename, extension_set):
                file_path = os.path.join(root, filename)
                packaged_files.append(file_path)

    logging.info('Packaged file list is: {0}'.format(packaged_files))
    return packaged_files


def do_unpack(from_dir, to_dir, extensions):
    logging.info('Unpack files from dir: {0} to dir: {1} with extensions: {2}'.format(from_dir, to_dir, extensions))

    Utils.rmdir(to_dir)
    Utils.mkdirs(to_dir)

    packaged_files = find_packaged_files(from_dir, extensions)
    for file_path in packaged_files:
        logging.debug('Unpack file: {0} to directory: {1}'.format(file_path, to_dir))

        if file_path.endswith('.zip'):
            Utils.unzip(file_path, to_dir)
        elif file_path.endswith('tar.gz') or file_path.endswith('.tar'):
            Utils.untar(file_path, to_dir)

    logging.info('Done unpack files.')


def parse_args():
    parser = argparse.ArgumentParser(description='Unpack files.')
    parser.add_argument('--from_dir', required=True, help='From dir.')
    parser.add_argument('--to_dir', required=True, help='To dir.')
    parser.add_argument('--extensions', required=False, help='Acceptable extensions.', default='.tar.gz,.tar,.zip')
    parser.add_argument('--log_level', help='Log Output Level', choices=['DEBUG', 'INFO', 'ERROR'], default='DEBUG')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    log_level = args.log_level

    Utils.log_configuration(log_level)
    logging.info('Input Parameters: {0}'.format(args))

    from_dir = args.from_dir
    to_dir = args.to_dir
    extensions = args.extensions

    do_unpack(from_dir, to_dir, extensions)
