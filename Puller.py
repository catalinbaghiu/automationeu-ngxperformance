import os
import argparse
from BenchProfile import *
from FtpHelper import *

def pullFromAndroid(component):
    resultDir = "/sdcard/ngx/cases"
    resultFile = "routingperformance.csv"

    if component.lower() == "guidance":
        resultFile = "GuidancePerformance.csv"

    reportPath = "result"
    if not os.path.exists(reportPath):
        os.mkdir(reportPath)
    os.system("adb pull {0} {1}".format(resultDir + "/" + resultFile, reportPath + "/performance.csv"))

def pullFromQnx(component, project):
    resultDir = "/fs/images/ngx/cases"
    resultFile = "routingperformance.csv"

    if component.lower() == "guidance":
        resultFile = "GuidancePerformance.csv"

    reportPath = "result"
    if not os.path.exists(reportPath):
        os.mkdir(reportPath)

    hostname, username, password, finish = getProfile(project)
    xfer = Xfer()
    xfer.setFtpParams(hostname, username, password)
    xfer.downloadfile(resultDir + "/" + resultFile, reportPath + "/performance.csv")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--component", required=True, help="Routing or Guidance")
    parser.add_argument("--project", required=True, help="gen3 or denali")
    
    args = parser.parse_args()
    component = args.component.strip()
    project = args.project.strip()

    if project.lower() == "denali":
        pullFromAndroid(component)
    elif project.lower() == "gen3":
        pullFromQnx(component, project)
    else:
        exit("We are not supporting this project: " + project)

    

