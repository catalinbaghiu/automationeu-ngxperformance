import argparse
from HTMLParser import HTMLParser
import urllib2
import os
import re
import sys
import logging

import Utils

BUFFER_SIZE = 128 * 1024
SPLIT_CHAR = ','

AWS_KOREA_REGION = 'ap-northeast-2'


def resolve_link(link, url):
    re_url = re.compile(r'^(([a-zA-Z_-]+)://([^/]+))(/.*)?$')

    m = re_url.match(link)
    if m is not None:
        if not m.group(4):
            # http://domain -> http://domain/
            return link + '/'
        else:
            return link
    elif link[0] == '/':
        # /some/path
        murl = re_url.match(url)
        return murl.group(1) + link
    else:
        # relative/path
        if url[-1] == '/':
            return url + link
        else:
            return url + '/' + link


class ListingParser(HTMLParser):
    """
    Parses an HTML file and build a list of links.
    Links are stored into the 'links' set. They are resolved into absolute links.
    """

    def __init__(self, url):
        HTMLParser.__init__(self)

        if url[-1] != '/':
            url += '/'
        self.__url = url
        self.links = set()

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for key, value in attrs:
                if key == 'href':
                    if not value:
                        continue
                    value = resolve_link(value, self.__url)
                    self.links.add(value)
                    break


def download_directory_via_http(url, target, extension_set):
    logging.debug('Parsing url: {0}'.format(url))

    def mkdir():
        if not mkdir.done:
            try:
                os.mkdir(target)
            except OSError:
                pass
            mkdir.done = True

    mkdir.done = False

    response = urllib2.urlopen(url)

    if response.info().type == 'text/html':
        contents = response.read()

        parser = ListingParser(url)
        parser.feed(contents)
        for link in parser.links:
            link = resolve_link(link, url).rstrip('/')
            if not link.startswith(url):
                continue
            name = link.rsplit('/', 1)[1]
            if '?' in name:
                continue
            mkdir()
            download_directory_via_http(link, os.path.join(target, name), extension_set)
        if not mkdir.done:
            # We didn't find anything to write inside this directory
            # Maybe it's a HTML file?
            if url[-1] != '/':
                end = target[-5:].lower()
                if not (end.endswith('.htm') or end.endswith('.html')):
                    target = target + '.html'
                with open(target, 'wb') as fp:
                    fp.write(contents)
    elif Utils.endswith_set(url, extension_set):
        logging.info('Downloading: {0} to: {1}'.format(url, target))
        with open(target, 'wb') as fp:
            chunk = response.read(BUFFER_SIZE)
            while chunk:
                fp.write(chunk)
                chunk = response.read(BUFFER_SIZE)
    else:
        logging.info('Skip download file: {0}'.format(url))


def download_file_via_http(url, todir):
    try:
        response = urllib2.urlopen(url)
        logging.debug('Downloading: {0} to: {1}'.format(url, todir))

        with open(os.path.join(todir, os.path.basename(url)), 'wb') as local_file:
            while True:
                chunk = response.read(BUFFER_SIZE)
                if not chunk:
                    break
                local_file.write(chunk)

    except urllib2.HTTPError, exception:
        logging.error('URL Error. Reason: {0} URL: {1}'.format(exception.reason, url))
        sys.exit(1)
    except urllib2.URLError, exception:
        logging.error('URL Error. Reason: {0} URL: {1}'.format(exception.reason, url))
        sys.exit(1)


def check_http_code(url):
    try:
        connection = urllib2.urlopen(url)
        status_code = connection.getcode()
        connection.close()
        return status_code
    except urllib2.HTTPError, exception:
        logging.error('URL Error. Reason: {0} URL: {1}'.format(exception.reason, url))
        sys.exit(1)


def parse_extension_set(extensions):
    extension_set = set()

    if SPLIT_CHAR in extensions:
        # have multiple extension list
        for extension in extensions.split(SPLIT_CHAR):
            extension_set.add(extension)
    else:
        # only one extension provide
        extension_set.add(extensions)

    logging.info('Accept extension is: {0}'.format(extension_set))
    return extension_set


def parse_url_list(urls):
    url_list = []
    if SPLIT_CHAR in urls:
        # have multiple urls
        for url in urls.split(SPLIT_CHAR):
            url_list.append(url.rstrip('/'))
    else:
        url_list.append(urls.rstrip('/'))

    logging.info('Input url list is: {0}'.format(url_list))
    return url_list


def download_via_s3(url, todir):
    logging.info('Downloading file from: {0} to: {1}'.format(url, todir))

    target_file = os.path.join(todir, os.path.basename(url))

    command = 'aws s3 --region ' + AWS_KOREA_REGION + ' cp ' + url + ' ' + target_file
    logging.debug('S3 command is: {0}'.format(command))
    Utils.execute_real_time_command(command)


def download(url_list, extension_set):
    logging.info('Download for url list: {0} with extensions: {1}'.format(url_list, extension_set))

    for url in url_list:
        logging.info('Parsing URL: {0}'.format(url))

        if url.startswith('s3://'):
            logging.info('Try downloading by s3 protocol for url: {0}'.format(url))

            download_via_s3(url, todir)
        elif url.startswith('http://') or url.startswith('https://'):
            logging.info('Try downloading by http protocol for url: {0}'.format(url))

            check_http_code(url)
            if Utils.endswith_set(url, extension_set):
                download_file_via_http(url, todir)
            else:
                download_directory_via_http(url, todir, extension_set)
        else:
            logging.error('Protocol not supported! {0}'.format(url))


def parse_args():
    parser = argparse.ArgumentParser(description='Download data.')
    parser.add_argument('--url', required=True, help='Urls')
    parser.add_argument('--todir', required=True, help='todir')
    parser.add_argument('--extensions', required=False, help='File with extension will download.', default='')
    parser.add_argument('--log_level', help='Log Output Level', choices=['DEBUG', 'INFO', 'ERROR'], default='DEBUG')

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()

    urls = args.url
    todir = args.todir
    extensions = args.extensions
    log_level = args.log_level

    Utils.log_configuration(log_level)
    logging.info('Input Parameters: {0}'.format(args))

    Utils.rmdir(todir)
    Utils.mkdirs(todir)

    extension_set = parse_extension_set(extensions)
    url_list = parse_url_list(urls)

    download(url_list, extension_set)
