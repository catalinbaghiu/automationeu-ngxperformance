# encoding=utf-8
import time
def do_telnet(Host, username, password, finish, commands):
    import telnetlib
    '''''Telnet loging：Windows connect to Linux'''

    # connect to telnet
    tn = telnetlib.Telnet(Host, port=23, timeout=10)
    tn.set_debuglevel(2)

    # input username
    tn.read_until('login: ')
    tn.write(username + '\n')

    # input password
    tn.read_until('Password:')
    tn.write(password + '\n')

    # login is successful, execute commands
    tn.read_until(finish)
    for command in commands:
        tn.write('%s\n' % command)
    
    time.sleep(1)

    # exit telnet connection after finishing
    tn.read_until(finish)
    tn.close()  # tn.write('exit\n')


if __name__ == '__main__':
    #pass
    do_telnet('192.168.1.26', 'root', '#Pasa3Ford', '# ', ['ls -la /fs/images'])