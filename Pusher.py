import os
import argparse
import logging
import ConfigParser
import re
from TelnetConnection import *
from BenchProfile import *
from FtpHelper import *
import time

BENCH_IP = "192.168.1.100"

def log_configuration(log_level):
    logging.basicConfig(format='%(levelname)s %(filename)s %(asctime)s %(funcName)s %(lineno)s:%(message)s', level=log_level)

def checkDeviceConnectivity():
    cmd = "adb devices > devices.txt"
    os.system(cmd)

    found = False
    file_object = open('devices.txt')
    for line in file_object.readlines():
        if line.find(BENCH_IP) != -1 and line.find("device") != -1:
            found = True
            break

    if not found:
        exit("ERROR: No device connected!")

def loadBuildConfig(key, project, modelYear):
    runner = ""
    config = ConfigParser.ConfigParser()
    config.read("build.ini")
    if config.has_section(key):
        runner = config.get(key, project + "_" + modelYear)
    return runner.strip()

def modify_config(file, target_data_path, target_suite_path, target_config_path, target_traffic_data_path, target_traffic_config_path, target_traffic_db_file, region, component):
    logging.info(target_data_path)
    logging.info(target_suite_path)
    logging.info(target_config_path)
    results = ""
    file_object = open(file, 'r')
    for line in file_object:
        
        if line.find("envpath") != -1:
            # update data config path
            data_config_path = "linux_data_config_path="
            if line.find(data_config_path) != -1:
                logging.info("Updating linux_data_config_path")
                line = re.sub("linux_data_config_path=.*?/tiles/access_config.json;",
                              "linux_data_config_path=" + target_data_path + "/tiles/access_config.json;", line)

            data_path = "linux_data_path="
            if line.find(data_path) != -1:
                logging.info("Updating linux_data_path")
                line = re.sub("linux_data_path=.*?/;", "linux_data_path=" + target_data_path + "/;", line)

            config_path = "linux_config_path="
            if line.find(config_path) != -1:
                logging.info("Updating linux_config_path")
                line = re.sub("linux_config_path=.*?/;", "linux_config_path=" + target_config_path + "/;", line)

            traffic_data_path = "linux_traffic_data_path="
            if target_traffic_data_path != "" and component == "routing" and region == "cn":
                if line.find(traffic_data_path) != -1:
                    logging.info("Updating linux_traffic_data_path")
                    line = re.sub("linux_traffic_data_path=.*?/;", "linux_traffic_data_path=" + target_traffic_data_path + "/;", line)
                else:
                    line = re.sub('value="', 'value="linux_traffic_data_path=' + target_traffic_data_path + '/;', line)

            traffic_config_path = "linux_traffic_config_path="
            if target_traffic_config_path != "" and component == "routing" and region == "cn":
                if line.find(traffic_config_path) != -1:
                    logging.info("Updating linux_traffic_config_path")
                    line = re.sub("linux_traffic_config_path=.*?/;", "linux_traffic_config_path=" + target_traffic_config_path + "/;", line)
                else:
                    line = re.sub('value="', 'value="linux_traffic_config_path=' + target_traffic_config_path + '/;', line)

            traffic_db_file = "linux_traffic_db_file="
            if target_traffic_db_file != "" and component == "routing" and region == "cn":
                if line.find(traffic_db_file) != -1:
                    logging.info("Updating linux_traffic_db_file")
                    line = re.sub("linux_traffic_db_file=.*?/;", "linux_traffic_db_file=" + target_traffic_db_file + "/;", line)
                else:
                    line = re.sub('value="', 'value="linux_traffic_config_path=' + target_traffic_db_file + '/;', line)

            testsuits_path = "testsuits_path="
            if line.find(testsuits_path) != -1:
                logging.info("Updating testsuits_path")
                line = re.sub("testsuits_path=.*?testsuites", "testsuits_path=" + target_suite_path, line)

            results = results + line
            continue
        results = results + line
    file_object.close()

    file_object = open(file, 'w')
    file_object.write(results)
    file_object.close()



def runOnAndroid(project, modelYear, region, component, fromDataDir, fromCasesDir, fromBuildDir, fromTrafficDir):

    runner = loadBuildConfig("cpu", project, modelYear)
    binPackage = loadBuildConfig("os", project, modelYear)
    targetDir = "/sdcard/ngx"
    targetDataDir = targetDir + "/map"
    targetCasesDir = targetDir + "/cases"
    targetConfigDir = targetDir + "/config"
    targetTrafficDir = targetDir + "/traffic"
    targetBuildDir = targetDir + "/build"

    logging.info("Modifying cregex_config.xml ...............")
    if project == "denali":
        modify_config("cregex_config.xml", targetDataDir, targetCasesDir + "/data/testsuites", targetConfigDir + "/" + region, targetTrafficDir + "/" + region, targetTrafficDir + "/" + region, "", region, component)
    else:
        modify_config("cregex_config.xml", targetDataDir, targetCasesDir + "/data/testsuites", targetConfigDir + "/" + region, "", "", "", region, component)
    # protect connection issue
    os.system("adb root")
    os.system("adb connect 192.168.1.100")
    time.sleep(2)
    os.system("adb connect 192.168.1.100")
    time.sleep(2)
    os.system("adb connect 192.168.1.100")
    os.system("adb remount")
    os.system("adb connect 192.168.1.100")
    checkDeviceConnectivity()
    logging.info('Clean up before pushing!............')
    os.system("adb shell rm -fr " + targetDir)

    logging.info('Pushing data onto device............ from {0} to {1}'.format(fromDataDir, targetDataDir))
    os.system("adb push " + fromDataDir + " " + targetDataDir)

    if region == "cn" and component == "routing":
        testsuites = os.listdir(fromCasesDir + os.sep + "data" + os.sep + "testsuites")
        for suite in testsuites:
            if suite.find("_" + project) == -1:
                logging.info("Deleting file...... {0}".format(fromCasesDir + os.sep + "data" + os.sep + "testsuites" + os.sep + suite))
                os.remove(fromCasesDir + "/data/testsuites/" + suite)
    
    logging.info("Pushing cases onto device............ from {0} to {1}".format(fromCasesDir, targetCasesDir))
    os.system("adb push " + fromCasesDir + " " + targetCasesDir)
    logging.info('Pushing regex onto device............ from {0} to {1}'.format(fromBuildDir + "/" + binPackage + "/bin/" + runner + "/RegEx", targetBuildDir))
    os.system("adb push " + fromBuildDir + "/" + binPackage + "/bin/" + runner + "/RegEx" + " " + targetBuildDir + "/RegEx")
    logging.info('Pushing config onto device............ from {0} to {1}'.format(fromBuildDir + "/" + binPackage + "/BackendConfig/" + project.lower(), targetConfigDir))
    os.system("adb push " + fromBuildDir + "/" + binPackage + "/BackendConfig/" + project.lower() + " " + targetConfigDir)
    logging.info('Pushing cregex_config.xml onto device............ from {0} to {1}'.format("cregex_config.xml", targetCasesDir + "/cregex_config.xml"))
    os.system("adb push " + "cregex_config.xml" + " " + targetCasesDir + "/cregex_config.xml")
    
    if project == "denali" and component == "routing" and region == "cn":
        logging.info('Pushing traffic config onto device............ from {0} to {1}'.format(fromTrafficDir + "/" + binPackage + "/BackendConfig/" + project.lower(), targetTrafficDir))
        os.system("adb push " + fromTrafficDir + "/" + binPackage + "/BackendConfig/" + project.lower() + " " + targetTrafficDir)


def runOnQnx(project, modelYear, region, component, fromDataDir, fromCasesDir, fromBuildDir, fromTrafficDir):
    hostname, username, password, finish = getProfile(project)

    logging.info('Building connection for host: {0}, user: {1}'.format(hostname, username))
    xfer = Xfer()
    xfer.setFtpParams(hostname, username, password)

    runner = loadBuildConfig("cpu", project, modelYear)
    binPackage = loadBuildConfig("os", project, modelYear)
    targetDir = "/fs/images/ngx"
    targetDataDir = targetDir + "/map"
    targetCasesDir = targetDir + "/cases"
    targetConfigDir = targetDir + "/config"
    targetTrafficDir = targetDir + "/traffic"
    targetBuildDir = targetDir + "/build"

    logging.info("Modifying cregex_config.xml ...............")
    if project == "gen3" and region == "cn" and component == "routing":
        modify_config("cregex_config.xml", targetDataDir, targetCasesDir + "/data/testsuites",
                      targetConfigDir + "/" + region, "", targetTrafficDir + "/" + region, targetCasesDir + "/data/tpeg_perf_cn.db", region, component)
    else:
        modify_config("cregex_config.xml", targetDataDir, targetCasesDir + "/data/testsuites",
                      targetConfigDir + "/" + region, "", "", "", region, component)

    logging.info('Clean up before pushing!............')
    do_telnet(hostname, username, password, finish, ["rm -fr " + targetDir])

    logging.info('Creating folders!............')
    do_telnet(hostname, username, password, finish, ["mkdir -p " + targetDataDir, "mkdir -p " + targetCasesDir, "mkdir -p " + targetConfigDir, "mkdir -p " + targetTrafficDir, "mkdir -p " + targetBuildDir])


    logging.info('Pushing regex onto device............ from {0} to {1}'.format(fromBuildDir + os.sep + binPackage + os.sep + "bin" + os.sep + runner + os.sep + "RegEx", targetBuildDir))
    xfer.upload(fromBuildDir + os.sep + binPackage + os.sep + "bin" + os.sep + runner + os.sep + "RegEx", targetBuildDir) 
    
    logging.info('Pushing data onto device............ from {0} to {1}'.format(fromDataDir, targetDataDir))
    xfer.upload(fromDataDir, targetDataDir)

    if region == "cn" and component == "routing":
        testsuites = os.listdir(fromCasesDir + os.sep + "data" + os.sep + "testsuites")
        for suite in testsuites:
            if suite.find("_" + project) == -1:
                logging.info("Deleting file...... {0}".format(
                    fromCasesDir + os.sep + "data" + os.sep + "testsuites" + os.sep + suite))
                os.remove(fromCasesDir + "/data/testsuites/" + suite)

    logging.info("Pushing cases onto device............ from {0} to {1}".format(fromCasesDir, targetCasesDir))
    xfer.upload(fromCasesDir, targetCasesDir)

    logging.info('Pushing config onto device............ from {0} to {1}'.format(fromBuildDir + os.sep + binPackage + os.sep + "BackendConfig" + os.sep + project.lower(), targetConfigDir))
    xfer.upload(fromBuildDir + os.sep + binPackage + os.sep + "BackendConfig" + os.sep + project.lower(), targetConfigDir)

    logging.info('Pushing cregex_config.xml onto device............ from {0} to {1}'.format("cregex_config.xml", targetCasesDir))
    xfer.upload("cregex_config.xml", targetCasesDir)

    if project == "gen3" and component == "routing" and region == "cn":
        logging.info('Pushing traffic config onto device............ from {0} to {1}'.format(fromTrafficDir + os.sep + binPackage + os.sep + "BackendConfig" + os.sep + project.lower(), targetTrafficDir))
        xfer.upload(fromTrafficDir + os.sep + binPackage + os.sep + "BackendConfig" + os.sep + project.lower(), targetTrafficDir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--component", required=True, help="Routing or Guidance")
    parser.add_argument("--project", required=True, help="denali or gen3")
    parser.add_argument("--modelYear", required=True, help="MY18 or MY19")
    parser.add_argument("--region", required=True, help="na or eu")
    parser.add_argument("--fromDataDir", required=True, help="From data location on PC.")
    parser.add_argument("--fromCasesDir", required=True, help="From case location on PC.")
    parser.add_argument("--fromBuildDir", required=True, help="From routing build location on PC.")
    parser.add_argument("--fromTrafficDir", required=True, help="From traffic build location on PC.")
    parser.add_argument('--log_level', help='Log Output Level', choices=['DEBUG', 'INFO', 'ERROR'], default='DEBUG')

    args = parser.parse_args()
    component = args.component.strip()
    fromDataDir = args.fromDataDir.strip()
    fromCasesDir = args.fromCasesDir.strip()
    fromBuildDir = args.fromBuildDir.strip()
    fromTrafficDir = ""
    if args.fromTrafficDir is not None:
        fromTrafficDir = args.fromTrafficDir.strip()
    project = args.project.strip()
    modelYear = args.modelYear.strip()
    region = args.region.strip()

    log_configuration(args.log_level.strip())
    logging.info('Input Parameters: {0}'.format(args))

    if project.lower() == "denali":
        logging.info("Android device................")
        runOnAndroid(project.lower(), modelYear.lower(), region.lower(), component.lower(), fromDataDir, fromCasesDir, fromBuildDir, fromTrafficDir)
    elif project.lower() == "gen3":
        logging.info("QNX device................")
        runOnQnx(project.lower(), modelYear.lower(), region.lower(), component.lower(), fromDataDir, fromCasesDir, fromBuildDir, fromTrafficDir)
    else:
        exit("We are not supporting this project: " + project)

